conda_base='/var/lib/cds-conda/base'
default_env='cds'

ID=`/usr/bin/id -u`
[ -n "$ID" -a "$ID" -lt 1000 ] && return

ssh_incoming_port=$(echo $SSH_CONNECTION | cut -d ' ' -f 4)
# if [ "${ssh_incoming_port}" == "22" -o -z "${ssh_incoming_port}" ] ; then
    if [ ! -e $HOME/.noconda ] ; then
        PS1="[\u@\h \W]\\$ "
        . ${conda_base}/etc/profile.d/conda.sh
        if [ -f ${conda_base}/etc/profile.d/mamba.sh ]; then
            . ${conda_base}/etc/profile.d/mamba.sh
        fi

        if [ ! -e $HOME/.noigwn ] ; then
            if [ -e $HOME/.conda_version ] ; then
                custom_env=$(head -1 $HOME/.conda_version)
            fi
            if [ -z "${custom_env}" ] ; then
                custom_env="${default_env}"
            fi

            if [ -n "${custom_env}" ] ; then
                conda activate $custom_env
            fi
        fi
    fi
# fi

