# cds-conda
Install and Maintain the CDS conda environment on debian systems

The package installs a systemd timer which starts a service each night that ensures
the latest CDS environment is installed.

Some scripts will auto-activate the environment in a new terminal.

## Install

Install from LIGO apt repostories using `apt-get install cds-conda`.

The environment is installed by a delayed installer. You could wait overnight, but to get the environment
earlier, run 'systemctl start cds-conda'.  This can take a half hour or more.

You'll need to log out or reboot after the environment is installed to get the auto-activation.

### Location

The conda installation and all environments are in `/var/lib/cds-conda/base`.

## Helpful commands

To drop out of conda, run `conda deactivate` repeatedly.

To check available enironments run `conda env list`. The starred environment is the currently active environment.

## Uninstallation

The environments take up a lot of space and take a long time to install. `apt remove cds-conda` won't delete them, but `apt purge cds-conda` will.


